import board.BoardMain;
import ui.UiDialog;

import java.util.Arrays;
import java.util.List;

/**
 *
 * Hauptklasse. Argumente für verschiedene Betriebsmodi.
 *
 * ARGUMENTE
 *
 * -mode=xxx        Startet Programm in bestimmtem Betriebsmodus (xxx=master/board/client)
 * -master_ip=xxx   xxx=IP-Adresse des Masters
 * -local_id=xxx    xxx=ID der lokalen Programm-Instanz (Board-ID/Client-ID, je nach Betriebsmodus)
 * -cmd             Startet Programm im interaktiven Konsolenmodus, abhängig von Betriebsmodus
 *
 * @author David Holzapfel
 */
public class Startup {

    private static final String ARG_BOARD_MODE = "-mode=board";
    private static final String ARG_CLIENT_MODE = "-mode=client";
    private static final String ARG_MASTER_MODE = "-mode=master";

    private static final String ARG_INTERACTIVE_MODE = "-cmd";

    private static final String REGEX_ARG_MASTER_IP = "-master_ip=.+";
    private static final String REGEX_ARG_LOCAL_ID = "-local_id=.+";

    private static final String INVALID_MODE_MESSAGE =
            "Invalid mode in argument list.";

    public static void main(String[] args) {
        List<String> argList = Arrays.asList(args);
        boolean interactiveMode = argList.contains(ARG_INTERACTIVE_MODE);

        String masterIp = "localhost";
        String localId = "-";
        for(String arg : argList) {
            if(arg.matches(REGEX_ARG_MASTER_IP)) {
                masterIp = arg.split("=")[1];
            }
            if(arg.matches(REGEX_ARG_LOCAL_ID)) {
                localId = arg.split("=")[1];
            }
        }

        //Board-Betrieb
        if(argList.contains(ARG_BOARD_MODE)) {
            new BoardMain(localId, masterIp, interactiveMode).loop();
        }
        //Client-Betrieb
        else if(argList.contains(ARG_CLIENT_MODE)) {
            new UiDialog().start();
        }
        //Master-Betrieb
        else if(argList.contains(ARG_MASTER_MODE)) {

        }
        //Kein Betriebsmodus gewählt
        else {
            System.out.println(INVALID_MODE_MESSAGE);
        }
    }

}
