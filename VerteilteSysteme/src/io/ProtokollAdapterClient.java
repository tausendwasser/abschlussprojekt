/**
 * 
 */
package io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * @author Sven Manier
 * 
 * Ein Adapter der auf das Protokoll des VerteileSysteme-Projekts zugeschnitten ist
 */
public class ProtokollAdapterClient extends ProtokollAdapter{
	private String tableIP;
	private String clientID;

	public ProtokollAdapterClient(String tableIP, String clientID) throws IOException {
		serverSocket = new ServerSocket(2048);
		this.clientID = clientID;
		this.tableIP = tableIP;
	}	

	

	/**
	 * Client -> Table
	 * @param loginIP
	 */
	public void sendClientLogin(String loginIP) {

	}
	
	/**
	 * Client -> Table
	 */
	public void sendClientLogout(){
		
	}
	
	/**
	 * Client -> Table
	 * @param message
	 * @param msgID
	 */
	public void sendPostMessage(String message, long msgID){
		
	}
	
	/**
	 * Client -> Table
	 * @param targetID
	 * @param message
	 * @param msgID
	 */
	public void sendPrivateMessage(String clientIDDestination, String message, long msgID){
		
	}
	
	/**
	 * Client -> Table
	 * @param message
	 * @param msgID
	 */
	public void sendGlobalMessage(String message, long msgID){
		
	}
}















