package io;

import java.io.IOException;
import java.net.ServerSocket;

public class ProtokollAdapterMaster extends ProtokollAdapter{
	private String masterID;
	
	public ProtokollAdapterMaster(String masterID) throws IOException {
		serverSocket = new ServerSocket(2048);
		this.masterID = masterID;
	}
	
	/**
	 * Master -> Table
	 * @param clientIDSource
	 * @param clientIDDestination
	 * @param message
	 * @param msgID
	 */
	public void sendBackup(String clientIDSource, String clientIDDestination, String message, long msgID){
		
	}
	
	/**
	 * Master -> Table, sendet immer nur 1 Tafel-Information
	 * @param tableID
	 * @param tableIP
	 */
	public void sendTableList(String tableID, String tableIP){
		
	}
}
