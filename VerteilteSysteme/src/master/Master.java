package master;

import board.Datenbank.Member;
import board.Datenbank.PrivateMessage;

import java.util.ArrayList;
import java.util.Hashtable;

public class Master 
{
	private static int masterId = 1;

	private String id;
	private String adress;

	private Hashtable<String, String> tables;
	private Hashtable<String, ArrayList<Member>> members;
	private ArrayList<PrivateMessage> backupPrivateMessages;

	public Master() 
	{
		tables = new Hashtable<String, String>();		//Tafelid + Adresse
		members = new Hashtable<String, ArrayList<Member>>();//Tafelid + liste der zugeh�rigen member
		backupPrivateMessages = new ArrayList<PrivateMessage>();
		//adresse besetzen

		id = "m0" + masterId;
		masterId++;
	}

	public String getAdress() {
		return adress;
	}
	
	public String getId() {
		return id;
	}

	public Hashtable<String, String> getTables() 
	{
		return tables;
	}

	public Hashtable<String, ArrayList<Member>> getMembers() 
	{
		return members;
	}
	
	public void registriereTafel(String id, String adresse)
	{
		tables.put(id, adresse);
		//tafelliste senden
	}
	
	public void entferneTafel(String id)
	{
		if (tables.containsKey(id))
			tables.remove(id);
	}
	
	public void speichereBackupNachricht(PrivateMessage PrivateMessage)
	{
		backupPrivateMessages.add(PrivateMessage);
	}
	
	public ArrayList<PrivateMessage> sendeBackupNachrichten(String empfaengerId)
	{
		ArrayList<PrivateMessage> benutzerListe = new ArrayList<PrivateMessage>();
		for (PrivateMessage m : backupPrivateMessages)
		{
			if (m.getRecipientID().equals(empfaengerId))
				benutzerListe.add(m);
		}
		return benutzerListe;
	}
	
	public void entferneBackupNachtichten(String empfaengerId)
	{	
		for (int i = 0; i< backupPrivateMessages.size();i++)
		{
			if (backupPrivateMessages.get(i).getRecipientID().equals(empfaengerId))
				backupPrivateMessages.remove(i);
		}		
	}
	
	public void registriereMember(String tafelId, Member member)
	{
		if (members.containsKey(tafelId))
		{
			members.get(tafelId).add(member);
		}
		else
			{
				ArrayList<Member> tempListe = new ArrayList<Member>();
				tempListe.add(member);
				members.put(tafelId, tempListe);
			}
	}

}
