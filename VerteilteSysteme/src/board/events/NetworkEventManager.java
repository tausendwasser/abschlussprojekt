package board.events;

import board.events.eventclasses.NetworkEvent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Verwaltet eingegangene Netzwerkevents und verteilt sie an alle
 * registrierten Event-Listener.
 *
 * @author David Holzapfel
 */
public class NetworkEventManager {

    /**
     * Updateverfahren, nach dem NetworkEvents abgearbeitet werden.
     * SINGLE:  Ein Event pro Update
     * ALL:     Alle Events pro Update
     */
    public enum UpdateMode {
        SINGLE,
        ALL
    }

    private static NetworkEventManager INSTANCE = null;

    /**
     * @return Die Instanz des NetworkEventManagers
     */
    public static NetworkEventManager getInstance() {
        if(INSTANCE == null)
            INSTANCE = new NetworkEventManager();
        return INSTANCE;
    }

    private List<NetworkEvent> networkEventQueue;
    private Map<Class, List<NetworkEventListener>> networkEventListeners;
    private UpdateMode updateMode;

    private NetworkEventManager() {
        this.networkEventQueue = new LinkedList<>();
        this.networkEventListeners = new HashMap<>();
        this.updateMode = UpdateMode.SINGLE;
    }

    /**
     * Setzt den Updatemodus des NetworkEventManagers.
     *
     * @param updateMode
     */
    public void setUpdateMode(UpdateMode updateMode) {
        this.updateMode = updateMode;
    }

    /**
     * Trägt ein NetworkEvent ein, das an alle NetworkEventListener weitergeleitet wird, die
     * sich für die entsprechente Eventklasse eingetragen haben.
     *
     * @param event    Das NetworkEvent-Objekt, das die Daten für das Event beinhaltet
     */
    public void raiseEvent(NetworkEvent event) {
        //Zugriff anderer Threads auf die Collection blockieren
        synchronized(networkEventQueue) {
            //Event in die Update-Liste eintragen
            networkEventQueue.add(event);
        }
    }

    /**
     * Trägt einen NetworkEventListener ein, sodass dieser benachrichtigt wird sobald ein
     * entsprechendes Event auftritt.
     *
     * @param listener      Das NetworkEventListener-Objekt
     * @param eventClass    Das Klassenobjekt der Eventklasse
     */
    public void addEventListener(NetworkEventListener listener, Class eventClass) {
        //Wenn noch kein Listener dieser Eventklasse vorhanden ist, wird eine leere Liste für diesen Typ
        //in die Map eingefügt
        if(!networkEventListeners.containsKey(eventClass)) {
            networkEventListeners.put(eventClass, new LinkedList<>());
        }
        //Listener in die Liste der entsprechenden Eventklasse eintragen
        networkEventListeners.get(eventClass).add(listener);

    }

    /**
     * Behandelt aufgetretene Netzwerkevents, indem es diese an die entsprechenden NetworkEventListener
     * weiterleitet.
     */
    public void update() {
        //Liste leer -> kein Update erforderlich
        if(networkEventQueue.isEmpty())
            return;

        //Standardmäßig ein Event pro Update
        int updates = 1;
        if(updateMode == UpdateMode.ALL) {
            updates = networkEventQueue.size();
        }

        //Zugriff anderer Threads auf diese Collection blockieren
        synchronized(networkEventQueue) {
            for(NetworkEvent event : networkEventQueue.subList(0, updates)) {
                for(NetworkEventListener listener : networkEventListeners.get(event.getClass())) {
                    listener.handleEvent(event);
                }
            }
            //Alle behandelten Events aus der Liste rauswerfen
            networkEventQueue.removeAll(networkEventQueue.subList(0, updates));
        }
    }

}