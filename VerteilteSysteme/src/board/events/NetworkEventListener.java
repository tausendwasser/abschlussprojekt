package board.events;

import board.events.eventclasses.NetworkEvent;

/**
 * Interface für einen Event-Handler, der Netzwerk-Events einer bestimmten Eventklasse
 * behandelt.
 *
 * @param <E> Eventklasse, die behandelt wird.
 *
 * @author David Holzapfel
 */
public interface NetworkEventListener<E extends NetworkEvent> {

    /**
     * Führt Code aus, der für ein Netzwerkevent einer bestimmten Klasse
     * geschrieben wurde.
     *
     * @param event Das Eventobjekt, welches die relevanten Daten beinhaltet
     */
    void handleEvent(E event);

}
