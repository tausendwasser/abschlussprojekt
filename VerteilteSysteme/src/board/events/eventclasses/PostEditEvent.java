package board.events.eventclasses;

public class PostEditEvent extends NetworkEvent {

    public String postId;
    public String newText;
    public boolean deletePost;

    public PostEditEvent(String postId, String newText) {
        this.postId = postId;
        this.newText = newText;
        this.deletePost = false;
    }

    public PostEditEvent(String postId) {
        this.postId = postId;
        this.newText = "";
        this.deletePost = true;
    }

}
