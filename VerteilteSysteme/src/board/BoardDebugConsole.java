package board;

/**
 * Behandelt Debug-Konsolenkommandos.
 *
 * @author David Holzapfel
 */
public class BoardDebugConsole {

    private static final String REGEX_NEW_POST =
            "^event new_post .*";

    public void handleLine(String line) {
        if(line.equals("exit")) {
            System.exit(0);
        }
        else {
            System.out.println("Unrecognized command: '" + line + "'");
        }
    }

}
