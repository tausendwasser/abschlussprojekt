package board;

import board.Datenbank.Board;
import board.Datenbank.Member;
import board.Datenbank.Post;
import board.Datenbank.PrivateMessage;
import io.ProtokollAdapterTable;

import java.io.IOException;

/**
 * Helferklasse, um den Netzwerkkram aus den Listenern herauszuabstrahieren.
 *
 * @author David Holzapfel
 */
public class NetworkHelper {

    private static ProtokollAdapterTable ADAPTER;

    public static void initHelper(String tableId, String masterIp) {
        try {
            ADAPTER = new ProtokollAdapterTable(tableId, masterIp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPostToMember(Post post, Member destination) {

    }

    public static void sendPMToMember(PrivateMessage msg, Member destination) {

    }

    public static void sendPMToExternalMember(PrivateMessage msg) {

    }

    public static void sendPostToBoard(Post post, Board destination) {

    }

}
