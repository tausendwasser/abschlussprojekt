package board.Datenbank;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
 * @author -Philip-
 * Klasse zum Speichern einer privaten Nachricht
 * 23.03.2016
 */

public class PrivateMessage {
	private String id;
	private String text;
	private String authorID;
	private String recipientID;
	private String time;
	
	public PrivateMessage (String id, String text, String authorID, String recipientID){
		this.id = id;
		this.text = text;
		this.authorID = authorID;
		this.recipientID = recipientID;
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		this.time = sdf.format(new Date());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getID() {
		return id;
	}

	public String getAuthorID() {
		return authorID;
	}

	public String getRecipientID() {
		return recipientID;
	}
	
	public String getTime(){
		return time;
	}
}
