package board.Datenbank;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Datenbank
 * @author Philip Weber
 * 28.03.2016
 */
public class Database {
	//Klassenattribute
	private ArrayList<Post> PostList;
	private ArrayList<Member> MemberList;
	private ArrayList<Board> BoardList;
	private ArrayList<PrivateMessage> PMList;
	
	//Klassenkonstanten
	public static final String POST_NOT_FOUND   = "Die Nachricht mit dieser ID kann nicht gefunden werden!";
	public static final String MEMBER_NOT_FOUND = "Der Member mit dieser ID kann nicht gefunden werden!";
	public static final String PM_NOT_FOUND     = "Die PN mit dieser ID kann nicht gefunden werden!";
	public static final String BOARD_NOT_FOUND  = "Die Tafel mit dieser ID kann nicht gefunden werden!";
	
	//Konstruktor
	public Database(){
		PostList   = new ArrayList<Post>();
		MemberList = new ArrayList<Member>();
		BoardList  = new ArrayList<Board>();
		PMList     = new ArrayList<PrivateMessage>();
	}
	
	/**
	 * Fuegt ein Postobjekt hinzu
	 * @param id, ID des zu erstellenden Post
	 * @param text, Inhalt des Post
	 * @param authorID, ID des Authors
	 * @param global, boolean ob Post tafelintern oder tafeluebergreifend ist
	 */
	public void addPost(String id, String text, String authorID, boolean global){
		Post newPost = new Post(id, text, authorID, global);
		PostList.add(newPost);
	}
	
	/**
	 * Methode um eine vorhandenen Post zu bearbeiten
	 * @param id, ID des Post
	 * @param newText, neue Version des Inhalts
	 * @throws ElementNotFoundException, falls Post nicht vorhanden
	 */
	public void editPost(String id, String newText) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < PostList.size(); i++){
			if(id.equals(PostList.get(i).getID())){
				PostList.get(i).setText(newText);
				found = true;
				break;
			}
		}
		if(found == false)
			throw new ElementNotFoundException(POST_NOT_FOUND);
	}
	
	/**
	 * Methode um eine vorhandenen Post zu loeschen
	 * @param id, ID des Post
	 * @throws ElementNotFoundException, falls Post nicht vorhanden
	 */
	public void removePost(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < PostList.size(); i++){
			if(id.equals(PostList.get(i).getID())){
				PostList.remove(i);
				found = true;
				break;
			}				
		}
		if (found == false)
			throw new ElementNotFoundException(POST_NOT_FOUND);
	}

	/**
	 *
	 * @param id, ID des Posts
	 * @return Post-Objekt
	 * @throws ElementNotFoundException, falls Post nicht vorhanden
	 */
	public Post getPostFromID(String id) throws ElementNotFoundException{
		for(int i = 0; i < PostList.size(); i++){
			if(id.equals(PostList.get(i).getID())){
				return PostList.get(i);
			}
		}
		throw new ElementNotFoundException(MEMBER_NOT_FOUND);

	}
	
	/**
	 * 
	 * @param id, ID des Authors
	 * @return Author als String
	 * @throws ElementNotFoundException, falls Author nicht vorhanden
	 */
	public String getAuthorFromID(String id) throws ElementNotFoundException{
		for(int i = 0; i < MemberList.size(); i++){
			if(id.equals(MemberList.get(i).getID())){
				return MemberList.get(i).getName();
			} 
		}
		throw new ElementNotFoundException(MEMBER_NOT_FOUND);
		
	}
	
	/**
	 * Methode um einen neuen Member zu registrieren
	 * @param id, ID des neuen Members
	 * @param name, Name des neuen Members
	 * @param admin, boolean ob der neue Member ein Admin ist oder nicht
	 */
	public void addMember(String id, String name, boolean admin){
		Member newMember = new Member(id, name, admin);
		MemberList.add(newMember);
	}
	
	/**
	 * Prueft ob ein Member mit diesem Namen existiert
	 * @param name, Name des Members
	 * @return MemberID
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public String doesMemberExist(String name) throws ElementNotFoundException{
		for(int i = 0; i < MemberList.size(); i++){
			if(name.equals(MemberList.get(i).getName()))
				return MemberList.get(i).getID();
		}
		throw new ElementNotFoundException(MEMBER_NOT_FOUND);
	}
	
	/**
	 * Methode um einen Member einzuloggen
	 * @param id, ID des Members
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public void loginMember(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < MemberList.size(); i++){
			if(id.equals(MemberList.get(i).getID())){
				MemberList.get(i).setOnline(true);
				found = true;
				break;
			}
			if(found == false)
				throw new ElementNotFoundException(MEMBER_NOT_FOUND);
		}
	}

	/**
	 *
	 * @param memberId, ID des Members
	 * @return Member als String
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public Member getMemberFromId(String memberId) throws ElementNotFoundException {
		for(int i = 0; i < MemberList.size(); i++){
			if(memberId.equals(MemberList.get(i).getID())){
				return MemberList.get(i);
			}
		}
		throw new ElementNotFoundException(MEMBER_NOT_FOUND);
	}
	
	/**
	 * Methode um einen Member auszuloggen
	 * @param id, ID des Members
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public void logoutMember(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < MemberList.size(); i++){
			if(id.equals(MemberList.get(i).getID())){
				MemberList.get(i).setOnline(false);
				found = true;
				break;
			}
			if(found == false)
				throw new ElementNotFoundException(MEMBER_NOT_FOUND);
		}
	}
	
	/**
	 * Methode um ein Member zu loeschen
	 * @param id, ID des Members
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public void removeMember(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < MemberList.size(); i++){
			if(id.equals(MemberList.get(i).getID())){
				MemberList.remove(i);
				found = true;
				break;
			}				
		}
		if (found == false)
			throw new ElementNotFoundException(MEMBER_NOT_FOUND);
	}
	/**
	 * Gibt alle Attribute eines Member zurueck
	 * @param name des Members
	 * @return Alle Attribute
	 * @throws ElementNotFoundException, falls Member nicht vorhanden
	 */
	public String memberToString(String name) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < MemberList.size(); i++){
			if(name.equals(MemberList.get(i).getName())){
				found = true;
				return MemberList.get(i).toString();
			}				
		}
		if (found == false)
			throw new ElementNotFoundException(MEMBER_NOT_FOUND);
		return null; 
	}

	/**
	 * Fuegt eine neue Tafel der Tafelliste hinzu
	 * @param id, ID der neuen Tafel
	 * @param adress, Adresse der neuen Tafel
	 * @param name, Name der neuen Tafel
	 */
	public void addNewBoard(String id, String adress, String name){
		Board newBoard = new Board(id, adress, name);
		BoardList.add(newBoard);
	}
	
	/**
	 * Methode um eine vorhandene Tafel zu loeschen
	 * @param id, ID der Tafel
	 * @throws ElementNotFoundException, falls Tafel nicht vorhanden
	 */
	public void removeBoard(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < BoardList.size(); i++){
			if(id.equals(BoardList.get(i).getID())){
				BoardList.remove(i);
				found = true;
				break;
			}				
		}
		if (found == false)
			throw new ElementNotFoundException(BOARD_NOT_FOUND);
	}
	
	/**
	 * Gibt alle Attribute einer Tafel zurueck
	 * @param id, ID der Tafel
	 * @return Alle Attribute
	 * @throws ElementNotFoundException, falls Tafel nicht vorhanden
	 */
	public String boardToString(String id) throws ElementNotFoundException{
		for(int i = 0; i < BoardList.size(); i++){
			if(id.equals(BoardList.get(i).getID())){
				return BoardList.get(i).toString();
			}				
		}		
		throw new ElementNotFoundException(BOARD_NOT_FOUND);
	}
	
	/**
	 * Methode um eine PN zu speichern (falls Empfaenger offline)
	 * @param id, ID der PN
	 * @param text, Inhalt der PN
	 * @param authorID
	 * @param recipientID
	 */
	public void savePM(String id, String text, String authorID, String recipientID){
		PrivateMessage newPM = new PrivateMessage(id, text, authorID, recipientID);
		PMList.add(newPM);
	}
	
	/**
	 * Methode um eine vorhandene PN zu loeschen
	 * @param id, ID der PN
	 * @throws ElementNotFoundException, falls PM nicht vorhanden
	 */
	public void removePM(String id) throws ElementNotFoundException{
		boolean found = false;
		for(int i = 0; i < PMList.size(); i++){
			if(id.equals(PMList.get(i).getID())){
				PMList.remove(i);
				found = true;
				break;
			}				
		}
		if (found == false)
			throw new ElementNotFoundException(PM_NOT_FOUND);
	}
	
	/**
	 * Gibt den Inhalt einer PN anhand ihrer ID zurueck
	 * @param id, ID der PN
	 * @return Inhalt
	 * @throws ElementNotFoundException, falls PN nicht vorhanden
	 */
	public String getPM(String id) throws ElementNotFoundException{
		for(int i = 0; i < PMList.size(); i++){
			if(id.equals(PMList.get(i).getID())){
				return PMList.get(i).getText();
			}				
		}
		throw new ElementNotFoundException(PM_NOT_FOUND);
	}

	public PrivateMessage getPMObject(String id) throws ElementNotFoundException {
		for(int i = 0; i < PMList.size(); i++){
			if(id.equals(PMList.get(i).getID())){
				return PMList.get(i);
			}
		}
		throw new ElementNotFoundException(PM_NOT_FOUND);
	}
	
	/**
	 * Liefert alle Posts
	 */
	public ArrayList<Post> getAllPosts(){
		return PostList;
	}
	
	/**
	 * Liefert alle Member
	 */
	public ArrayList<Member> getAllMembers(){
		return MemberList;
	}
	
	/**
	 * Liefert alle Boards
	 */
	public ArrayList<Board> getAllBoards(){
		return BoardList;
	}
	
	/**
	 * Liefert alle PNs
	 */
	public ArrayList<PrivateMessage> getAllPMs(){
		return PMList;
	}
	
	/**
	 * Liefert ein Objekt anhand seiner ID unabhaengig seines Typs
	 * @param id, ID des Objekts
	 * @return Objekt mit entsprechender ID
	 * @throws ElementNotFoundException, falls Objekt nicht vorhanden
	 */
	public Object findElementByID(String id) throws ElementNotFoundException{
		for(int i = 0; i < PostList.size(); i++){
			if(id.equals(PostList.get(i).getID())){
				return PostList.get(i);
			}				
		}
		for(int i = 0; i < MemberList.size(); i++){
			if(id.equals(MemberList.get(i).getID())){
				return MemberList.get(i);
			}				
		}
		for(int i = 0; i < BoardList.size(); i++){
			if(id.equals(BoardList.get(i).getID())){
				return BoardList.get(i);
			}				
		}
		for(int i = 0; i < PMList.size(); i++){
			if(id.equals(PMList.get(i).getID())){
				return PMList.get(i);
			}				
		}
		throw new ElementNotFoundException("Das Objekt mit dieser ID kann nicht gefunden werden!");
	}
}