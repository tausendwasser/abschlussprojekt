package board.Datenbank;

/**
 * 
 * @author -Philip-
 * Klasse zum Speichern einer Tafel
 * 23.03.2016
 */

public class Board {
	private String id;
	private String adress;
	private String name;
	
	public Board(String id, String adress, String name){
		this.id = id;
		this.adress = adress;
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getID() {
		return id;
	}
	
	public String getName(){
		return name;
	}
}
