package board.Datenbank;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
 * @author -Philip-
 * Klasse zum Speichern einer Tafelnachricht
 * 23.03.2016
 */

public class Post {
	private String id;
	private String text;
	private String authorID;
	private boolean global;
	private String time;
	
	public Post (String id, String text, String authorID, boolean global){
		this.id = id;
		this.text = text;
		this.authorID = authorID;
		this.global = global;
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		this.time = sdf.format(new Date());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getID() {
		return id;
	}

	public String getAuthorID() {
		return authorID;
	}
	
	public boolean isGlobal(){
		return global;
	}
	
	public String getTime(){
		return time;
	}
}
